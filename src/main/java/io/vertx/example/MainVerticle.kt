package io.vertx.example

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.core.logging.LoggerFactory
import io.vertx.kotlin.core.json.*
import io.vertx.ext.auth.oauth2.*
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.CookieHandler
import io.vertx.ext.web.handler.OAuth2AuthHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.handler.UserSessionHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import io.vertx.kotlin.ext.auth.oauth2.OAuth2ClientOptions
import io.vertx.ext.web.templ.HandlebarsTemplateEngine

@Suppress("unused")
class MainVerticle : AbstractVerticle() {

    val logger = LoggerFactory.getLogger(this.javaClass.name)

    // In order to use a template we first need to create an engine
    private val engine = HandlebarsTemplateEngine.create()

    // This is an "generic" authProvider  (Could have used Google, Keycloack ... specifics ones)
    var oauth2 = OAuth2Auth.create(vertx, OAuth2FlowType.AUTH_CODE, OAuth2ClientOptions(
            clientID = "vertx-web",
            clientSecret = "12345678-edbf-4270-9959-62a62135b4ab", // "2c1845eb-edbf-4270-9959-62a62135b4ab", // Code not used ???
            site = "http://localhost:8080/auth/realms/demo/",
            tokenPath = "protocol/openid-connect/token",            // Path (=EndPoints) are relative to the site!
            authorizationPath = "protocol/openid-connect/auth"))

    // This is kind of a helper function to construct the URL
    var authorization_uri = oauth2.authorizeURL(json {
        obj(
                "redirect_uri" to "http://localhost:7080/callback",
                "scope" to "openid",
                "state" to "3(#0/!~")   // Supposed to track the state (create a new one, validate ...)
    })


    override fun start(startFuture: Future<Void>) {


        val router = Router.router(vertx)

        // We need a user session handler too to make sure the user is stored in the session between requests
        router.route().handler(CookieHandler.create())
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)))


        // The front-channel interaction aims at obtaining an Authorization Code that
        // should be use by this WebApp to query the ID Token directly to the OpenId Provider via back-channel
        //   This back-channel call is where the client secret is used, (an AccessToken is supposed to be delivered alongside the IDToken)


        // Default routes
        router.route().handler(UserSessionHandler.create(oauth2))

        // Protected routes  (is "/protected/*" required?)
        router.route("/protected").handler(
                // This will trigger the redirect to Authorization Server when this route is requested
                OAuth2AuthHandler.create(oauth2)
                        // OAuth2AuthHandler will manage the callback, we do not have to handle this route.
                        .setupCallback(router.route("/callback"))   // Does not seem to be used ??
                        // for this resource we require that this WebApp has the authority to retrieve the user emails
                        .addAuthority("user:email"))

        // Handlers, paths ...
        router.get("/").handler(handlerRoot)
        router.get("/protected/islands").handler(handlerIslands)
        router.get("/countries").handler(handlerCountries)


        vertx.createHttpServer()
                .requestHandler { router.accept(it) }
                .listen(config().getInteger("http.port", 7080)) { result ->
                    if (result.succeeded()) {
                        startFuture.complete()
                    } else {
                        startFuture.fail(result.cause())
                    }
                }
    }


    //
    // Handlers

    val handlerRoot = Handler<RoutingContext> { ctx ->
        logger.info("Root handler ... ")
        // we pass the client id to the template
        // WHY: Just to print the info
        ctx.put("client_id", "CLIENT_ID")
        // and now delegate to the engine to render it.
        engine.render(ctx, "views", "/index.hbs") { res ->
            logger.info("Root handler ... res: ", res.toString())
            if (res.succeeded()) {
                ctx.response()
                        .putHeader("Content-Type", "text/html")
                        .end(res.result())
            } else {
                ctx.fail(res.cause())
            }
        }
    }


    val handlerIslands = Handler<RoutingContext> { ctx ->
        logger.info("handlerIslands ... ")

        // This route is handled by Auth2, so we should already have been redirected to the AS server and back.
        // But still, the user is not Authenticated, because this is no OpenID Connect

        // Where in this API can I get the Authorization Code ??
        var code = "xxxxxxxxxxxxxxxxxxxxxxxx"

        oauth2.authenticate(
                json { obj(
                    "code" to code)}                 // Types and argument's names could help sometimes ...
        ) { res ->
            if (res.failed()) {
                // error handling...
            } else {
                var token = res.result()            // That would be the AccessToken ? or IDToken ??  or ??
            }
        }

        var user = ctx.user() as AccessToken
        // retrieve the user profile, this is a common feature but not from the official OAuth2 spec
        user.userInfo { res ->
            if (res.failed()) {
                // request didn't succeed because the token was revoked so we
                // invalidate the token stored in the session and render the
                // index page so that the user can start the OAuth flow again
                ctx.session().destroy()
                ctx.fail(res.cause())
            } else {
                // the request succeeded, so we use the API to fetch the user's emails
                var userInfo = res.result()

                ctx.response().endWithJson(MOCK_ISLANDS)
            }
        }
    }

    val handlerCountries = Handler<RoutingContext> { req ->
        req.response().endWithJson(MOCK_ISLANDS.map { it.country }.distinct().sortedBy { it.code })
    }


    //
    // Mock data

    private val MOCK_ISLANDS by lazy {
        listOf(
                Island("Kotlin", Country("Russia", "RU")),
                Island("Stewart Island", Country("New Zealand", "NZ")),
                Island("Cockatoo Island", Country("Australia", "AU")),
                Island("Tasmania", Country("Australia", "AU"))
        )
    }

    //
    // Utilities

    /**
     * Extension to the HTTP response to output JSON objects.
     */
    fun HttpServerResponse.endWithJson(obj: Any) {
        this.putHeader("Content-Type", "application/json; charset=utf-8").end(Json.encodePrettily(obj))
    }
}